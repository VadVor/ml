﻿
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.AspNetCore.Mvc;
using System.Threading;
using System.Threading.Tasks;
using WebSiteRankServer.Entities;


namespace WebSiteRankServer.Services
{

    public class DayVisitsService : IDayVisitsService
    {
        private DayVisitsDBContext _ctx;

        public DayVisitsService(DayVisitsDBContext ctx)
        {
            _ctx = ctx;
        }

        //consider pagination
        public async Task<IEnumerable<DayVisits>> GetAllRec()
        {
            return await _ctx.Records.ToListAsync();

        }

        //consider pagination
        public async Task<IEnumerable<DayVisits>> GetAllRecForTheDate(string date)
        {

            return await _ctx.Records.Include(t => t.date).ToListAsync();

        }
        //To 5
        public async Task<IEnumerable<DayVisits>> GetTop5RecForTheDate(string date)
        {

            return await _ctx.Records.Include(t => t.date).OrderByDescending(t => t.visits).Take(5).ToListAsync();

        }
        //get single record 
        public async Task<IEnumerable<DayVisits>> GetSingleRec(string date, string site)
        {
            return await _ctx.Records.Include(t => t.date).Include(t => t.site).ToListAsync();

        }

        // disable async warning - no RemoveAsync available
        public async Task DelRecForTheSiteForTheDate(string dt, string st)
        {
            DayVisits ent = new DayVisits(dt,st);
            _ctx.Records.Remove(ent);
        }

        public async Task AddNewRec(string date, string site, int vs)
        {
            var ent = await GetSingleRec(date, site);
            if (ent != null)
            {
                throw new Exception($"Cannot add: the record exists");
            }
            var dv = new DayVisits(date,site,vs);
            _ctx.Records.Add(dv);
        }

        public async Task ModifyRec(string dt, string st, int vs)
        {
            var ent = await GetSingleRec(dt, st);
            if (ent == null)
            {

                throw new Exception($"Cannot modify: record doesnt exist");
            }
            var dv = new DayVisits(dt,st,vs);
            _ctx.Records.Add(dv);
        }

        public async Task<bool> SaveAsync()
        {
            return (await _ctx.SaveChangesAsync() >= 0);
        }


    }
}
