﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WebSiteRankServer.Entities;

namespace WebSiteRankServer.Services
{
    public interface IDayVisitsService
    {
        Task AddNewRec(string dt, string st, int vs);
        Task DelRecForTheSiteForTheDate(string dt, string st);
        Task<IEnumerable<DayVisits>> GetAllRec();
        Task<IEnumerable<DayVisits>> GetAllRecForTheDate(string dt);
        Task<IEnumerable<DayVisits>> GetSingleRec(string dt, string st);
        Task<IEnumerable<DayVisits>> GetTop5RecForTheDate(string dt);
        Task ModifyRec(string dt, string st, int vs);
        Task<bool> SaveAsync();
    }
}