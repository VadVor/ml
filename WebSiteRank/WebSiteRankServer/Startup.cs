﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using WebSiteRankServer.Services;
using Swashbuckle.AspNetCore.Swagger;
using WebSiteRankServer.Entities;

namespace WebSiteRankServer
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services
            .AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1)
            .AddJsonOptions(options =>
                        {
                            options.SerializerSettings.DateParseHandling = DateParseHandling.DateTimeOffset;
                            options.SerializerSettings.ContractResolver =
                                new CamelCasePropertyNamesContractResolver();
                        });
            //CORS to  allow API allows requests from JavaScript: for now all origins/headers/methods are allowed.  
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAllOriginsHeadersAndMethods",
                    builder => builder.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod());
            });

            // register the DbContext on the container
            var connectionString = Configuration["ConnectionStrings:DefaultConnection"];
            //var connectionString = @"Server=wbss.database.windows.net,1433;Database=WebStatDB;User ID=usradmin;Password=xxxxxxxx";
            services.AddDbContext<DayVisitsDBContext>(o => o.UseSqlServer(connectionString));
            

            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "WebSiteRank API", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILogger<Startup> logger)
        {

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), 
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "WebSiteRank API");
            });

            //TO DO : remove tracing 
            app.Use(next =>
            {
                return async context =>
                {
                    logger.LogInformation(" -> Env:{0} Request:{1}", env.EnvironmentName, context.Request.Path);
                    await next(context);
                };
            });

            app.UseDefaultFiles();
            app.UseStaticFiles();

            if (env.IsDevelopment())
            {

                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            //replace wwwroot with WebSiteRank\WebSiteRankClient\dist\WebSiteRankClient

            app.UseHttpsRedirection();
            // Enable CORS
            app.UseCors("AllowAllOriginsHeadersAndMethods");
            app.UseMvc();
        }
    }
}
