﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebSiteRankServer.Entities;
using WebSiteRankServer.Services;
using AutoMapper;

namespace WebSiteRankServer.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class RankController : ControllerBase
    {
        //DB 
        private IDayVisitsService _dvs;


       // GET: api/Rank
        [HttpGet(Name = "GetAll")]
        public async Task<IActionResult> Get()
        {
            var res = await _dvs.GetAllRec();
            Mapper.Map<IEnumerable<DayVisits>>(res);
            return Ok(res);
        }

        // GET: api/Rank/2018-10-23    - top five
        [HttpGet("{dt}", Name = "GetTop5")]
        public async Task<IActionResult> Get( string dt)
        {
            var res = await _dvs.GetTop5RecForTheDate(dt);
            if (res == null)
            {
                return BadRequest();
            }
            return Ok(Mapper.Map<IEnumerable<DayVisits>>(res));

        }

        // GET: api/Rank/2018-10-23/cnn.com
        [HttpGet("{dt}/{st}", Name = "GetOne")]
        public async Task<IActionResult> Get(string dt, string st)
        {
            var res = await _dvs.GetSingleRec(dt,st);
            if (res == null)
            {
                return BadRequest();
            }
            return Ok(Mapper.Map<IEnumerable<DayVisits>>(res));
        }

        // POST: api/Rank
        [HttpPost ("{dt}/{st}")]
        public async Task<IActionResult> Post(string dt, string st, int vs )
        {

            await _dvs.AddNewRec(dt, st, vs);
            if (!await  _dvs.SaveAsync())
            {
                throw new Exception("Adding a tour failed on save."); ;
            }

            return CreatedAtRoute("", "aaa", " ");
            /*
               new { tourId = tourToReturn.TourId },
               tourToReturn);
               */
        }

        // PUT: api/Rank/2018-10-23/cnn.com - modify entry
        [HttpPut("{dt}/{st}")]
        public async Task<IActionResult> PutAsync(string dt, string st, int id)
        {

            await _dvs.ModifyRec(dt, st, id);
            
            if (!await _dvs.SaveAsync())
            {
                throw new Exception("Adding entry failed on save.");
            }
            return CreatedAtRoute("", "aaa", " ");

            //return "Post /" + dt + "/" + st;
        }

        // DELETE: api/Rank/2018-10-23/cnn.com - remove entry
        [HttpDelete("{id}")]
        public string Delete(string dt, string st)
        {
            return "Post /" + dt + "/" + st;
        }
    }
}
