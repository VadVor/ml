﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebSiteRankServer.Entities
{
    public class DayVisits
    { 
        /*
            [Key]
            [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
            */
        [Required]
        [MaxLength(256)]
            public string site { get; set; }
        [Required]
        public string date { get; set; }
        [Required]
        public int visits { get; set; }
        public string datetime { get; set; }
        public DayVisits(string dt, string st, int vs = 1)
        {
            date = dt;
            site = st;
            visits = vs;
        }
    }
}
