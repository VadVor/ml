﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.ChangeTracking;
namespace WebSiteRankServer.Entities
{

    public class DayVisitsDBContext : DbContext
    {
        //added to initialize DBconnection here
        public DayVisitsDBContext(DbContextOptions<DayVisitsDBContext> opt): 
            base(opt)
        {
            Database.EnsureCreated();
        }





        public DbSet<DayVisits> Records { get; set; }

        //to initialize DBconnection
        /*
        protected override void OnConfiguring(DbContextOptionsBuilder opt)
        {
            opt.UseSqlServer(opt);
        }
        */


        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            // get added or updated entries
            var addedOrUpdatedEntries = ChangeTracker.Entries().Where(x => (x.State == EntityState.Added || x.State == EntityState.Modified));
            // fill out the audit fields if/when needed

            return base.SaveChangesAsync(cancellationToken);
        }


    }

}
