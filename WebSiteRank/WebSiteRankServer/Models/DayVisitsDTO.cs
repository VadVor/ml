﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebSiteRankServer.Models
{
    public class DayVisitsDTO
    {
        public string site { get; set; }
        public string date { get; set; }
        public int visits { get; set; }
        public string datetime { get; set; }
    }
}
