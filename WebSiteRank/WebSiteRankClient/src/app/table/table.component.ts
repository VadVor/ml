import { Component, OnInit } from '@angular/core';
import { TableService } from './table.service';
import { Table } from './Table';



@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css'],
  template: '<ng2-smart-table [settings]="settings" [source]="data"></ng2-smart-table>'
  })

  //mock data to show the table
export class TableComponent {
  settings = {
    columns: {
      dt: {
        title: 'Date'
      },
      ws: {
        title: 'WebSite'
      },
      vs: {
        title: 'Visitors'
      },
      upd: {
        title: 'Updated'
      }
    }
  };

  data = [
    {
    ws:"www.bbc.com",
    dt:"2018-10-09",
    vs:"1234567",
    upd:"2018-11-11"
    },
    {
    ws:"www.bbc.com",
    dt:"2018-10-10",
    vs:"1268768767",
    upd:"2018-11-12"
    },
    {
    ws:"www.bbc.com",
    dt:"2018-10-11",
    vs:"122234567",
    upd:"2018-11-11"
    },
    {
    ws:"www.cnn.com",
    dt:"2018-10-09",
    vs:"1235887",
    upd:"2018-11-11"
    },
    {
    ws:"www.cnn.com",
    dt:"2018-10-10",
    vs:"134200767",
    upd:"2018-11-12"
    },
    {
    ws:"www.cnn.com",
    dt:"2018-10-11",
    vs:"125567",
    upd:"2018-11-11"
    },
    {
    ws:"www.thestar.com",
    dt:"2018-10-09",
    vs:"8835887",
    upd:"2018-11-11"
    },
    {
    ws:"www.thestar.com",
    dt:"2018-10-10",
    vs:"9340767",
    upd:"2018-11-12"
    },
    {
    ws:"www.thestar.com",
    dt:"2018-10-11",
    vs:"1559967",
    upd:"2018-11-12"
    },
    {
    ws:"www.lemonde.fr",
    dt:"2018-10-09",
    vs:"77635887",
    upd:"2018-11-11"
    },
    {
    ws:"www.lemonde.fr",
    dt:"2018-10-10",
    vs:"2440767",
    upd:"2018-11-12"
    },
    {
    ws:"www.lemonde.fr",
    dt:"2018-10-11",
    vs:"15580767",
    upd:"2018-11-12"
    }
    ];
    }

