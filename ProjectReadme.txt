-----------------------------------------
WebSiteRank project approach
-----------------------------------------
Building SPA application with packaging website as a set of static assets and host as static web pages
and 
having a separate set of back-end services that the application communicates with
approach chosen for the project.

Con: 
you have to manage two separate projects and debug them separately as well

Pro:
Decoupling technologies, more flexibility
i.e if it will be ReactJS instead of Angular used as front end
or if server side technologies will be changed.

-----------------------------------------
Pre-requirements  .NET CORE 2.1
-----------------------------------------

.NET CORE 2.1
To check which version of .NET CORE you have run in command line
.NET CORE
"powershell (dir (Get-Command dotnet).Path.Replace('dotnet.exe', 'shared\Microsoft.NETCore.App')).Name"
SDK
"powershell(dir (Get-Command dotnet).Path.Replace('dotnet.exe', 'sdk')).Name"

-----------------------------------------
WebSiteRank project structure hierarhy
-----------------------------------------
The code provided as hierarhy:

WebSiteRank
	WebSiteRankServer            //WebAPI 
	WebSiteRankClient            //Angular Front End
        DBSetUp                      //DB setup scripts and instruction 
				       scripts and CSV file to load into DB  
                                      NOTE: DB currently configured to run on Azure  


The projects scaffolding has been done as follows:
1.
mkdir WebSiteRank

2.Create Angular Client hierarhy
PreRequirement NodeJS has to be installed

cd WebSiteRank
ng new WebSiteRankClient

3. Build and/or serve Client

cd WebSiteRank\WebSiteRankClient

To get NPM packages run 
npm install 

To get code built to 
WebiteRank\WebSiteRankClient\dist

To get code  packaged to 
WebiteRank\WebSiteRankClient\dist
and verify if the build is OK 
ng serve

webserver run as http://localhost:4200/
open browser to navigate 

4. Create Server hierarhy
cd WebSiteRank
mkdir WebSiteRankServer
cd WebSiteRankServer
dotnet new webapi 

5. Build server
dotnet build



-------------------------------------------------
Currently only DB is on Azure  
moving WebApi and Client to Azure is in progress
-------------------------------------------------

NOTES:
1.
Microsoft Azure account 
After you provide all the details and when your account is set you will get welcome email 
with link to logon https://azure.microsoft.com/en-ca/get-started/ 

2.For new projects install latest Azure CLI
On Windows install 
https://docs.microsoft.com/en-us/cli/azure/install-azure-cli-windows?view=azure-cli-latest
NOTE:As of Dec 2,2018 Latest version Azure-cli-2.0.51.msi  

3.
Install it, by default it is in: 
"C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Microsoft Azure



