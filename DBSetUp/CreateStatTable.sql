use WebStatDB
GO

IF EXISTS ( SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.WebSiteVisits') AND type in (N'U'))
DROP TABLE dbo.WebSiteVisits 
GO

create table dbo.WebSiteVisits (
	site varchar (256) NOT NULL,
	dt date            NOT NULL,
	visits int         NOT NULL,
	updTime datetime   NOT NULL default GETDATE(),
	PRIMARY KEY (site, dt)
) 
GO
