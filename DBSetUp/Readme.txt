--------------------------------------------------------
AZURE DB specific (skip if you have accesss to MSSQL DB )
Pre-Requirment:
AZURE account and AZURE CLI installed 
--------------------------------------------------------
1.Login into your account
az login
By default you will get Browser Window where you can enter credentials 


2.To create DB server on  Azure in case you dont have server
az sql server create 
--name wbss 
--resource-group  WebStatsGroup 
--location "Canada Central" 
--admin-user XXXXXXX 
--admin-password XXXXXXXXX


3.To create  DB on the server 
az sql db create 
--resource-group WebStatsGroup 
--server wbss 
--name WebStatDB 
--service-objective S0


4.When created you can access using to access use:
Server=tcp:wbss.database.windows.net,1433;
Database=WebStatDB;
User ID=XXXXXXX;
Password=XXXXXXXX;
Encrypt=true;
Connection Timeout=30;

5.To verify connection and DB access 
telnet wbss.database.windows.net 1433

6. To allow access from any host set firewall rules
az sql server firewall-rule 
az sql server firewall-rule create 
-g  WebStatsGroup
-s wbss
-n all 
--start-ip-address 0.0.0.0 --end-ip-address 255.255.255.255
SPECIAL NOTE:
when setup changed the rules limit access

-------------------------------------------
DB setup scripts
------------------------------------------- 
SQL scripts in the directory will allow  to create WebSiteVisits table 
and load CSV file(s)            
	site varchar (256) NOT NULL,
	dt date            NOT NULL,
	visits int         NOT NULL,
	updTime datetime   NOT NULL default GETDATE(),
	PRIMARY KEY (site, dt)	

the restriction put is you can have only one record for the website for the date 

SPECIAL NOTE: 
(site,dt) varchar(256) + date 
is not the best key combination
if had time I would add separate 
'id' - 'site'  table and yused the ids here   


	SITE		DT				VISITS	UPDTIME

	www.bbc.com	2018-10-09T00:00:00.0000000	1234567	2018-12-03T16:28:27.8330000
	
	www.bb.com	2018-10-09T00:00:00.0000000	1234567	2018-12-03T16:31:43.3730000

1.Create main(only) table
sqlcmd CreateStatTable.sql
to create table WebSiteVisits (it will drop and recreate table if exists)

2. Run BCP command to load test data
BCP  WebStatDB.dbo.WebSiteVisits in "c:\data\stats.csv" -t,  

Special note:
Since it is Azure SQL used for the project 
BCP was used for test data bulk insert 
NOTE:
For MSSQL could be used "BULK INSERT" command

Details are here:
https://docs.microsoft.com/en-us/sql/tools/bcp-utility?view=sql-server-2017

comma is the field separator specified for BSP as '-t,'  
CSV file  can be found in the same folder
